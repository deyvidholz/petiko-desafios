<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}



Documentação:
https://postmon.com.br/


*/

class CEP
{

    public static $apiURL = 'https://api.postmon.com.br/v1/';
    public static $decode = true;

    public static function getAddressByCep($cep)
    {
        /*
         * 
         * Retorna um JSON (string) se o $decode for false ou um array object se $decode for true.
         * Caso o retorno da API não seja 200 (OK) retorna false.
         * 
         */

          $url = sprintf('%s/cep/%s', self::$apiURL, $cep);
          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_URL, $url);
          // Desabilitando output
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

          $res = curl_exec($ch);
          // Pegando código de retorno
          $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);

          if ($statusCode === 200) {
            return self::$decode ? json_decode($res, true) : $res;
          }

        return false;
    }
}

var_dump(CEP::getAddressByCep('13450420'));