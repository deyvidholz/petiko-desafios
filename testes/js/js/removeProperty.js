// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.


/* Nota: por favor, acesse o arquivo ./removeProperty.html para uma melhor experiência */

function removeProperty(obj, prop) {
  if (obj.hasOwnProperty(prop)) { // ou obj[prop]
    delete obj[prop]
    alert('[TRUE] Seu objeto: ' + JSON.stringify(obj))
    return true
  }
  alert('[FALSE] Propriedade nao encontrada')
  return false;
}

const str = prompt('Crie um JSON valido (Ex: { "nome": "Petiko", "local": "SP" } )').replace(/\s/g, '')
const obj = JSON.parse(str)

const prop = prompt('Agora, escolha uma propriedade do seu objeto para remover - digite o nome dela:').trim()

removeProperty(obj, prop)