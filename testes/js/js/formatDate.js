// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD
  const regex = new RegExp(/^(\d{2})\/(\d{2})\/(\d{4})$/)
  if (userDate.trim().match(regex)) {
    // Método 1: RegExp
    const formattedDate = userDate.replace(regex, '$3$1$2')
    
    // Método 2: Split
    let formattedDate2 = userDate.split('/')
    formattedDate2 = `${formattedDate2[2]}${formattedDate2[0]}${formattedDate2[1]}`

    // Método para datas no formato brasileiro (DD/MM/YYY)
    // const formattedDate2 = userDate.split('/').reverse().join() // output: 20141231

    alert(`
      Data que voce enviou: ${userDate}\n
      Data formatada: ${formattedDate}
    `)

    return
  }

  alert(`A data informada ( ${userDate} ) é invalida`)
}

// console.log(formatDate("12/31/2014"));
// Descomente a linha acima ou acesse o arquivo ./formatDate.html