<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizanod a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a funação playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0, 
                'score' => 0
            );
        }
	}
		
	public function recordResult($player, $score)
  {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}
	
	public function playerRank($rank)
    {
      // Copiando array para não modificar o array original, que contém a ordem de adição
      $this->standingsCopy = $this->standings;

      // Colocando o nome do jogador no array cópia para fins de identificação.
      $playersRank = array();
      foreach ($this->standingsCopy as $index => $player) {
        $playersRank[$index]['player'] = $index;
        $playersRank[$index]['games_played'] = $player['games_played'];
        $playersRank[$index]['score'] = $player['score'];
      }

      // Apagando cópia temporária do array standings da memória
      unset($this->standingsCopy);


      // USORT: sorteando arrays
      usort($playersRank, function($player1, $player2) {

        // Verifica se o score e games_played são iguais, caso sejam, retorna false
        // Ou seja, mantém o primeiro jogador adicionado ao array.
        // Coloque return true para retornar o 2º jogador (nesse caso, Arnold)
        if ($player1['games_played'] === $player2['games_played'] && $player1['score'] === $player2['score']) return false;

          // Abaixo é utilizado o operador (PHP 7) "Spaceship", é como se fosse um operador de comparação múltipla.
          // Retorna 0 se os valores forem iguais, 1 caso o primeiro valor seja maior que o último
          // (resumindo, se $player1 for maior que $player2) e -1 do contrário.
          return [$player1['games_played'], $player1['score']]
              <=>
              [$player2['games_played'], $player2['score']];
      });

      return $playersRank[$rank] ? $playersRank[$rank]['player'] : 'Jogador não encontrado';
	}
}
      
$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);