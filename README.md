# Petiko - Processo seletivo.

Petiko é uma Startup com sede em São Carlos que atua no segmento Pet com milhares de clientes espalhados em todo o Brasil. Nosso objetivo é ajudar as pessoas a cuidarem melhor de seus animais de estimação e entregar felicidade. Para isso, contamos com diferentes canais e serviços:


* **BLOG.Petiko:** Portal de conteúdo que movimenta centenas de milhares de petlovers todos os meses
* **BOX.Petiko:** Maior clube de assinaturas Pet da américa latina. Entregamos mensalmente uma caixinhas com mimos e novidades para nossos assinantes
* **SHOP.Petiko:** Plataforma de compra exclusivo para assinantes BOX.Petiko

# Sobre a vaga!

Queremos um dev talentoso, motivado e que esteja disposto a aprender diariamente. Que seja multidiciplinar, e encare o desafio de transformar-se em um **Full Stack**.
Você irá trabalhar com:

* PHP, Laravel
* MySQL e bancos não relacionais
* Integrações de plataformas via REST API's
* Desenvolvimento de ferramentas de gestão e análises de processos
* Métodos de pagamento online e logística
* Usabilidade, testes e desenvolvimento de novas plataformas de vendas

**A vaga:**

* **Desenvolvedor Junior, Backend PHP.**
* **Presencial, Sâo Carlos SP**
* **Home Office**
* **Formato de contratação CLT.**

# O Processo seletivo - ETAPA 1
Para participar do processo seletivo, convidamos você a resolver a primeira etapa do processo seletivo.

**Dê um fork neste repositório e resolva os testes**.
Assim que **terminar**, nos envie o link para o bitbucket **do projeto com as respostas** para o e-mail dev@petiko.com.br.

Nesta etapa o objetivo é analisar um pouco de sua familiaridade com algorítimos e programação.
Cada teste possui um peso. Você não é obrigado a entregar todos os testes resolvidos, porém, ele será utilizado como critério para a eleição a próxima fase.

Boa sorte!!!

# Desafios resolvidos - Petiko
Segue todos os desafios resolvidos. Apenas algumas observações:

* Foi utilizado a versão **7** do PHP para a resolução dos mesmos.
* Utilizei VSCode, com identação de 2 espaços. Charset UTF-8.
* Preferi fazer de um jeito diferente, como por exemplo: em alguns momentos eu defino um array da seguinte forma: **$meuArray = array();** e em outros - **$meuArray = []**; apenas para deixar claro que conheço as duas sintaxes, mas sempre sigo um padrão.
* Utilizei funcionalidades do PHP 7 como por exemplo, definir array como **$meuArray = []** e o Spaceship Operator (**<=>**).
* Deixei comentado algumas linhas do código que pensei sem interessante.
* Os códigos JavaScript foram testados no navegador Opera, mas não se preocupe, Opera usa a mesma engine do Chrome (V8).
* Não utilizei jQuery nos exemplos. Mas conheço à biblioteca.
* Nos desafios de JavaScript, em todos eles, criei um arquivo HTML e inseri os arquivos **.js**. Em alguns desafios, também utilizei **Expressões Regulares (Regex)**, mas também conheço outras formas de fazer um **replace**.
* Qualquer outro desafio, fiquem à vontade :)