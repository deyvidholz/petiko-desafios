# Desafios resolvidos - Petiko
Segue todos os desafios resolvidos. Apenas algumas observações:

* Foi utilizado a versão **7** do PHP para a resolução dos mesmos.
* Utilizei VSCode, com identação de 2 espaços. Charset UTF-8.
* Preferi fazer de um jeito diferente, como por exemplo: em alguns momentos eu defino um array da seguinte forma: **$meuArray = array();** e em outros - **$meuArray = []**; apenas para deixar claro que conheço as duas sintaxes, mas sempre sigo um padrão.
* Utilizei funcionalidades do PHP 7 como por exemplo, definir array como **$meuArray = []** e o Spaceship Operator (**<=>**).
* Deixei comentado algumas linhas do código que pensei sem interessante.
* Os códigos JavaScript foram testados no navegador Opera, mas não se preocupe, Opera usa a mesma engine do Chrome (V8).
* Não utilizei jQuery nos exemplos. Mas conheço à biblioteca.
* Nos desafios de JavaScript, em todos eles, criei um arquivo HTML e inseri os arquivos **.js**. Em alguns desafios, também utilizei **Expressões Regulares (Regex)**, mas também conheço outras formas de fazer um **replace**.
* Qualquer outro desafio, fiquem à vontade :)